$(function(){

  /* firstview slider */
  if($('.firstview__slider')[0]){
    $('.firstview__slider').slick({
      dots: true,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 3000,
      fade: true,
      speed: 1000,
      pauseOnFocus: false
    });
  }


  /* Access Ranking */
  if($('.container-toppage-ranking')[0]){
    $('.container-toppage-ranking.monthly dl').each(function(i){
      var myCode = '<span class="ribbon num">' + (i+1) + '</span>'
      $(myCode).appendTo($(this));
    });
    $('.container-toppage-ranking.weekly dl').each(function(i){
      var myCode = '<span class="ribbon num">' + (i+1) + '</span>'
      $(myCode).appendTo($(this));
    });
    $('.container-toppage-ranking.total dl').each(function(i){
      var myCode = '<span class="ribbon num">' + (i+1) + '</span>'
      $(myCode).appendTo($(this));
    });
  }

  //lazyloadを強制load
  function forceLoad_lazy(obj){
    obj.find('.lazy').each(function(){
      if(!$(this).hasClass('loaded')){
        LazyLoad.load($(this)[0]);
      }
    })
  }

  /* expert-toppage */
  if($('.container-toppage-expert.slider')[0]){
    $('.container-toppage-expert.slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
      $('.container-toppage-expert.slider').find('.slick-active').each(function(){
        forceLoad_lazy($(this));
      })
    });
    $('.container-toppage-expert.slider').slick({
      slidesToShow: 5,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 3000,
      speed: 1000,
      responsive: [
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 1100,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 850,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '30%'
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '22%'
          }
        },
        {
          breakpoint: 430,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '18%'
          }
        },
        {
          breakpoint: 350,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '15%'
          }
        }
      ]
    });
  }


  /* publications-toppage */
  if($('.container-slider-publication')[0]){
    function responsive_slider(slider, settings){
      if($(window).width() > 1300) {
        if (slider.hasClass('slick-initialized')){
          slider.off('afterChange');
          slider.slick('unslick');
        }
        return
      }
      if(!slider.hasClass('slick-initialized')){
        slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
          slider.find('.slick-active').each(function(){
            forceLoad_lazy($(this));
          })
        });
        return slider.slick(settings);
      }
    }
    var publicationSlider = $('.container-slider-publication');
    var publicationSettings = {
      slidesToShow: 3,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 3000,
      speed: 1000,
      responsive: [
        {
          breakpoint: 850,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '30%'
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '22%'
          }
        },
        {
          breakpoint: 430,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '18%'
          }
        },
        {
          breakpoint: 350,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '15%'
          }
        }
      ]
    }

    responsive_slider(publicationSlider, publicationSettings); //ロード時にスライダー判定

    $(window).on('resize', function(){
      responsive_slider(publicationSlider, publicationSettings); //リサイズ時にスライダー判定
    });
  }

})
