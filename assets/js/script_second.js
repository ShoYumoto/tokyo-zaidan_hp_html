$(function(){

  /* ページ内アンカー */
  $('a[href^="#"]').on("click", function(){
    var href_anchor = $(this).attr("href");
    var headerHight = $('.global_header').outerHeight();
    var current = $(window).scrollTop();

    if(!$(href_anchor)[0]){ //nameの場合
      var myname = href_anchor.replace('#', '');
      var target_anchor = $('[name='+ myname + ']');
    }else{
      var target_anchor = $(href_anchor == "#" || href_anchor == "" ? 'html' : href_anchor);
    }
    var target_pos = target_anchor.offset().top;
    if(current > target_pos){
      target_pos = target_pos - headerHight;
    }
    $('body,html').animate({scrollTop:target_pos}, 400, 'swing');
    return false;
  });


  /* event-記事なし */
  if($('.container-event')[0]){
    if($('.container-event').find('.event').length == 0){
      $('.container-event').addClass('nocontent');
    }
  }


  /* 写真のない研究員用処置 */
  if($('.column-article')[0]){
    $('.column-article').find('.expert').each(function(){
      if($(this).find('.expert__ph img').length == 0){
        $(this).find('.expert__ph').css({
          'margin': 'auto',
          'width': 'auto'
        });
      }
    })
  }

})
