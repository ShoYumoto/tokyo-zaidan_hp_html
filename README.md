東京財団HP（jp, en）
===================

## URL

**JP**

https://www.tkfd.or.jp/

**EN（現状ドメイン）**

http://www.tokyofoundation.org/en/

-----

## ツー・ファイブ テストアップURL

https://tkfd.two-five.info/

PW, ID: 共通のもの

-----

## 記事中に挿入される画像レイアウトと付与するクラス、タグについて

**画像回り込み左寄せ：**

```
<img class="image-left" ...>
```

**画像回り込右寄せ：**

```
<img class="image-right" ...>
```

**キャプション付き画像：**

```
<dl class="captioned">
<dt><img src="..." alt=""></dt>
<dd class="image-caption">キャプションキャプションキャプション...</dd>
</dl>
```

**キャプション付き画像（画像回り込み左寄せ）：**

```
<dl class="captioned image-left">
<dt><img src="..." alt=""></dt>
<dd class="image-caption">キャプションキャプションキャプション...</dd>
</dl>
```

**キャプション付き画像（画像回り込み右寄せ）：**

```
<dl class="captioned image-right">
<dt><img src="..." alt=""></dt>
<dd class="image-caption">キャプションキャプションキャプション...</dd>
</dl>
```
